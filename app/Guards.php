<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Guards
 *
 * @property int $id
 * @property string $name
 * @property string|null $phonenumber
 * @property string|null $idnumber
 * @property string|null $email
 * @property int|null $client_id
 * @property string $employedDate
 * @property string|null $terminationDate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereEmployedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereIdnumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards wherePhonenumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereTerminationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Client $client
 * @property string|null $bank_account
 * @property string|null $employmentID
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereBankAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guards whereEmploymentID($value)
 */
class Guards extends Model
{
    use SoftDeletes;
    protected $fillable = ['guard_id', 'startDate', 'client_id'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id')->withDefault(function () {
            return null;
        });
    }

    public function assignments()
    {
        return $this->hasMany(Assignment::class, 'guard_id', 'id');
    }

    public function currentassignments()
    {
        return $this->hasMany(Assignment::class, 'guard_id', 'id')->whereNull('endDate');
    }

    public function standBy()
    {
        return $this->hasOne(Standby::class, 'id', 'guard_id');
    }
}
