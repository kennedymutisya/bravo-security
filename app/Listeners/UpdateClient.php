<?php

namespace App\Listeners;

use App\Client;
use App\Events\ClientTerminated;

class UpdateClient
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Handle the event.
     *
     * @param  ClientTerminated $event
     * @return void
     */
    public function handle(ClientTerminated $event)
    {

        // Find the client and then perform an update.

        $client = $this->client->find($event->client->id);

        $client->endDate = $event->request->date;
        $client->saveOrFail();
    }
}
