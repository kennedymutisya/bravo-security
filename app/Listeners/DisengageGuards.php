<?php

namespace App\Listeners;

use App\Assignment;
use App\Client;
use App\Events\ClientTerminated;
use Illuminate\Http\Request;

class DisengageGuards
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $client;
    protected $request;

    public function __construct(Client $client, Request $request)
    {
        $this->client = $client;
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  ClientTerminated $event
     * @return void
     */
    public function handle(ClientTerminated $event)
    {
        $assignment = $this->client::with(['soldiers' => function ($query) {
            $query->whereNull('endDate');
        }])->where('id', $event->client->id)->get();
        foreach ($assignment as $soldier) {
            foreach ($soldier->soldiers as $user) {
                $user->endDate = $event->request->date;
                $user->status = 'inactive';
                $user->saveOrFail();
            }


        }

        //dd($assignment);
    }
}
