<?php

namespace App\Http\Controllers;

use App\Payments;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payments $payments
     * @return \Illuminate\Http\Response
     */
    public function show($payments)
    {
        $payment = Payments::find($payments);
        $payment->load(['clients','payment']);
//        dd($payment);
        return view('payments.index',compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payments $payments
     * @return \Illuminate\Http\Response
     */
    public function edit( $payments)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Payments $payments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $payments)
    {
       $payment = Payments::find($payments);
        $payment->invoice = $request->invoice;
        $payment->save();
        return response()->json($payment);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payments $payments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payments $payments)
    {
        //
    }
}
