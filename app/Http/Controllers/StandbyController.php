<?php

namespace App\Http\Controllers;

use App\Standby;
use Illuminate\Http\Request;

class StandbyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Standby  $standby
     * @return \Illuminate\Http\Response
     */
    public function show(Standby $standby)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Standby  $standby
     * @return \Illuminate\Http\Response
     */
    public function edit(Standby $standby)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Standby  $standby
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Standby $standby)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Standby  $standby
     * @return \Illuminate\Http\Response
     */
    public function destroy(Standby $standby)
    {
        //
    }
}
