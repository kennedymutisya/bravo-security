<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AssignmentController extends Controller {
    protected $assignment;

    public function __construct(Assignment $assignment)
    {
        $this->assignment = $assignment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientguards = Client::find($request->clientid)->guards;

        $assignedguards = $this->assignment::all()->where('client_id', $request->clientid)->where('endDate', NULL)->count();

        $this->validate($request, [
            'guardid' => [
                function ($attribute, $value, $fail) use ($clientguards, $assignedguards)
                {
                    if ($clientguards == $assignedguards | $assignedguards > $clientguards)
                    {
                        $fail("This client requires only {$clientguards} guards");
                    }
                },
                function ($attribute, $value, $fail) use ($clientguards, $assignedguards)
                {
                    if (\DB::table('assignments')->where('guard_id', $value)->whereNull('endDate')->count() >= 1)
                    {
                        $fail("This guard cannot take a Client.");
                    }
                }
            ]
        ]);
        $this->assignment->client_id = $request->clientid;
        $this->assignment->guard_id = $request->guardid;
        $this->assignment->startDate = Carbon::today();
        $this->assignment->saveorFail();

        $this->assignment->mkono()->update([
            'client_id' => $request->clientid,
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Assignment $assignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Assignment $assignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assignment $assignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
        //
    }
}
