<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Client;
use App\Guards;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;

/**
 * Class GuardsController
 * @package App\Http\Controllers
 */
class GuardsController extends Controller
{
    protected $guards;
    protected $client;

    public function __construct(Guards $guards, Client $client)
    {

        // Terminate a guard
        // Suspend a guard
        // Rehire a guard
        // Guard Attendance
        // Guard Recommendtion letter
        // Guard Termination

        $this->guards = $guards;
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Schedule $schedule)
    {
        $guards = $this->guards::whereHas('assignments', function ($query) {
            $query->whereNull('endDate');

        })->with('client')->get();
        $guards = $this->guards::whereHas('currentassignments', function ($query) {
            $query->whereMonth('startDate','09');

        })->with('client')->get();
        return view('Guard.index', compact('guards'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assigns()
    {

        return view('Guard.assign');
    }

    public function unassignedguard()
    {
        // We will get every guard who does not have an assignment and any guard who is not assigned.
        $guards = $this->guards::whereNull('client_id')->paginate();
//        $guards = $this->guards::all();
        return view('Guard.unassigned', compact('guards'));
    }

    /**
     * @param Guards $guards
     * @return \Illuminate\Http\RedirectResponse
     */
    public function disengage(Guards $guards)
    {
        // We will ensure that the guard does not work for the client anymore and is free to be re-assigned or
        // fired by ending their assignment with their current client.

        $employee = $guards::with(['assignments', 'client'])->where('id', $guards->id)->first();
        $employee->assignments()->update([
            'endDate' => Carbon::today()
        ]);
        $employee->client_id = NULL;
        $employee->save();
        return back();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Guard.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
            'phonenumber' => ['required', 'unique:guards,phonenumber'],
            'idnumber' => ['required', 'unique:guards,idnumber'],
            'employeddate' => ['required', 'date'],
            'employmentID' => ['required', 'unique:guards,employmentID'],
            'email' => ['nullable', 'unique:clients,email'],
        ]);

        $this->guards->name = $request->name;
        $this->guards->phonenumber = $request->phonenumber;
        $this->guards->idnumber = $request->idnumber;
        $this->guards->employedDate = $request->employeddate;
        $this->guards->email = $request->email;
        $this->guards->bank_account = $request->bank_account;
        $this->guards->employmentID = $request->employmentID;
        $this->guards->saveOrFail();
        return redirect()->route('guard.show', [$this->guards->id]);
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($guards)
    {
        // Get the current station of the guard and
        // the history of his whereabouts since he was employed.
        $client = new Client();
        $client->clientswithoutguards();
        
        $guard = $this->CurrentWorkplace($guards);

        $history = $this->GuardHistory($guards);

        $clientswithoutGuards = $this->client->clientswithoutguards();


        return view('Guard.show', compact('guard', 'history', 'clientswithoutGuards'));
    }

    /**
     * @param $guards
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    protected function CurrentWorkplace($guards)
    {
        return Guards::with([
            'assignments' => function ($query) {
                $query->whereNull('endDate');
                $query->with('client');
            }
        ])->where('id', $guards)->first();
    }

    /**
     * @param $guards
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    protected function GuardHistory($guards)
    {
        return Guards::with([
            'assignments' => function ($query) {
                $query->whereNotNull('endDate');
                $query->with('trashedClient');
            }
        ])->where('id', $guards)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guards $guards
     * @return \Illuminate\Http\Response
     */
    public function edit(Guards $guards)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Guards $guards
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guards $guards)
    {
        $guards = $guards::where('id', $request->id)->first();
        $guards->name = $request->name;
        $guards->phonenumber = $request->phonenumber;
        $guards->idnumber = $request->idnumber;
        $guards->employedDate = $request->employeddate;
        $guards->email = $request->email;
        $guards->bank_account = $request->bank_account;
        $guards->employmentID = $request->employmentID;
        $guards->saveOrFail();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guards $guards
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guards $guards){

        $guards->delete();

        return back();
    }
}
