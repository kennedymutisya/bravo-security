<?php

namespace App\Console\Commands;

use App\Client;
use Illuminate\Console\Command;

class ClientMonthlyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bravo:allclients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'We will get all the clients this month with the all the workers who worked for them.';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $clients;

    /**
     * ClientMonthlyReport constructor.
     * @param $clients
     */
    public function __construct(Client $clients)
    {
        $this->clients = $clients;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allClients = $this->clients::with(['assignment'=> function ($query) {
//            $query->whereBetween();
        }]);
    }
}
