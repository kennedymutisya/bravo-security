<?php

namespace App\Console\Commands;

use App\Client;
use App\Payments;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Validation\Concerns\ValidatesAttributes;

class BillClients extends Command
{
    use ValidatesAttributes;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bravo:monthlyjob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bill every client on the first day of the month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $payments;

    public function __construct(Payments $payments)
    {
        parent::__construct();
        $this->payments = $payments;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Client $client)
    {
        // On the first day of every month, we will bill the client for
        // services rendered taking into account the client status. i.e,
        // if they have terminated or suspended our services.

        $clients = $this->getAllClients($client);

//        dd($clients->count());
//
        foreach ($clients as $payment) {
            $payments = new Payments();
            $payments->client_id = $payment->id;
            $payments->amountpaid = $payment->dailyrate;
            $payments->startofmonth = \Carbon\Carbon::now()->startOfMonth()->subMonth();
            $payments->endofmonth = \Carbon\Carbon::now()->endOfMonth()->subMonth();
            $payments->saveOrFail();
        }
    }

    protected function getAllClients($client)
    {
        return $this->ActiveClients($client);
    }

    /**
     * @param Client $client
     */
    protected function ActiveClients($client)
    {
        return $client::whereDoesntHave('payments', function ($query) {
            $query->where('startofmonth', \Carbon\Carbon::now()->startOfMonth()->subMonth());
        })->get();

        //return $client::all();
    }

    protected function billClients()
    {

    }
}
