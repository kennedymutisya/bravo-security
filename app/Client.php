<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Client
 *
 * @property int $id
 * @property string $name
 * @property int $guards
 * @property string $startDate
 * @property string $endDate
 * @property int $dailyrate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereDailyrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereGuards($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $phonenumber
 * @property string $email
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Client wherePhonenumber($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Guards[] $soldiers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Payments[] $payments
 */
class Client extends Model
{
    use SoftDeletes;
    protected $fillable = ['*',
    ];

    public function soldiers()
    {
        return $this->hasMany(Assignment::class, 'client_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(Payments::class, 'client_id', 'id');
    }

    public function assignment()
    {
        return $this->hasMany(Assignment::class, 'client_id', 'id');
    }

    public function clientswithoutguards()
    {
      return  $this::whereDoesntHave('soldiers')->whereNull('endDate')->get();
    }

    public function attendance()
    {
        return $this->hasManyThrough(Attendance::class,Guards::class,'client_id','guard_id');
    }
}