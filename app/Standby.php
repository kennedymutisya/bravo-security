<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standby extends Model
{
    public function standby()
    {
        return $this->belongsTo(Guards::class);
    }
}
