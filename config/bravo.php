<?php
/**
 * Created by PhpStorm.
 * User: Kennedy Mutisya
 * Date: 11/24/2017
 * Time: 2:19 AM
 */

return [

    'bravo' => [
        'slogan1' => 'Ordinary days require extra-ordinary protection.',
        'slogan2' => 'Hacking a human brain is easier than hacking a computer! Don\'t let your employees be the weakest link.'
    ]

];