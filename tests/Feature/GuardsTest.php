<?php

namespace Tests\Feature;

use App\Guards;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class GuardsTest extends TestCase {
    use WithoutMiddleware, RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddGuard()
    {
        // Post the Guard Data to the Guard route.
        // Then we will save the data and return back.
        $response = $this->post(route('guard.store'), [
            "name" => "Esta Little Jr.",
            "phonenumber" => "738.267.7683 x2842",
            "idnumber" => '30823861',
            "email" => "carmela55@gmail.com",
            "employeddate" => "2017-02-12 14:39:13.000000",
            "employmentID" => "BR-1990",
        ]);
        $response->assertStatus(302);
    }

    public function testIndexGets_all_active_guards()
    {
        factory(Guards::class)->states('assigned')->create();
    }

}
