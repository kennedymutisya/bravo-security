<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */


$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phonenumber' => $faker->phoneNumber,
        'email' => $faker->email,
        'guards' => $faker->numberBetween(1, 10),
        'startDate' => $faker->dateTimeThisMonth(),
        'endDate' => $faker->dateTimeThisMonth(),
        'dailyrate' => $faker->numberBetween(300, 340),
    ];
});
