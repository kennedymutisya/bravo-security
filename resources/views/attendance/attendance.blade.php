<html>
<head>
    <title>Bravo Security</title>
    <script language="JavaScript" type="text/javascript">
        /*setTimeout("window.print();", 10000);*/
    </script>
    <style>
        body {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
        }

        table.data {
            font-family: Verdana;
            font-size: 9px;
            empty-cells: show;
            border: 1px solid #000;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        table.data td {
            border: 1px solid black;
        }

        table.data td.header {
            background-color: #EDECEB;
            font-size: 13px !important;
            font-weight: bold;
        }

        table.data td.abottom {
            vertical-align: bottom;
            /*font-size: 10px;*/
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px;
            height: 50px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }


    </style>
</head>
<body>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td colspan="9">

            <table width="100%" border=0 cellspacing="0" cellpadding="1" class="data">

                <tr>
                    <td rowspan="6" align="center">
                        <img src="{{ asset('BRAVOLOGO.png') }}" height="100px">
                    </td>
                </tr>
                <?php /** @var \App\Settings $settings */ ?>
                <tr>
                    <td valign="top" colspan="2"><span class="title">Bravo Two Zero Security Service LTD</span></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Address : P.O BOX 132 &mdash; 90138, Makindu</b></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Tel : 0722 393 529 | 0713 642 175</b></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Your security is our commitment! </b></td>
                </tr>

                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>

                <tr>
                    <td><b>ATTENDANCE</b></td>
                    <td colspan="4"><b>5 SEPTEMBER, 2018</b></td>
                </tr>

            </table>

        </td>
    </tr>

</table>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td nowrap class="header"></td>
        <td nowrap class="header">NAME</td>
        <td nowrap class="header">
            Present
        </td>
        <td nowrap class="header">Absent</td>
        <td nowrap class="header">Sick</td>
        <td nowrap class="header">Off</td>
        <td nowrap class="header">A.O.R</td>
        <tbody>

        @foreach($client as $clients)
            <tr>
                <td colspan="7"><b>{{ strtoupper($clients->name) }}</b></td>
            </tr>
            @foreach($clients->attendance as $cl)
                <tr>
                    <td align="right">{{ $cl->soldier->employmentID }}</td>

                    <td>{{ $cl->soldier->name }}
                        @if($cl->soldier->employmentID === 'BR-1867')
                            <span style="float: right !important;">
                                <strong>STANDBY-GUARD</strong>
                            </span>
                        @endif
                    </td>
                    <td align="center"><strong>PRESENT</strong></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
        @endforeach

        </tbody>
</table>
</body>
</html>

