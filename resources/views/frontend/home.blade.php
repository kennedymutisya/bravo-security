@extends('layouts.frontend')

@section('content')
    <div id="owl-main-slider" class="main-slider owl-carousel enable-owl-carousel" data-single-item="true"
         data-pagination="false" data-auto-play="true" data-main-slider="true" data-stop-on-hover="true">
        <div class="item">
            <img src="{{ asset('frontend/img/BravoCCTV.png') }}" alt="slider">
            <div class="container-fluid">
                <div class="row">
                    <div class="slider-content col-md-5">
                        <h1 class="main-slider__title">GUARANTEE YOUR SAFETY IN ANY SITUACION IN YOUR LIFE </h1>
                        <p class="main-slider__content">At Bravo Security, we guarantee top notch security and
                            unbrindled safety to you anywhere, all the time</p>
                        <a class="main-slider__btn btn btn-success" href="#">LEARN MORE</a>
                        <div class="main-slider__control">
                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="{{ asset('frontend/img/BravoCCTValt.png') }}" alt="slider">
            <div class="container-fluid">
                <div class="row">
                    <div class="slider-content col-md-5">
                        <h1 class="main-slider__title">CCTV and Alarm Specialists </h1>
                        <p class="main-slider__content">Should you have need for anything to do with CCTV or alarms
                            installed
                            on any premises. We are just the guys you need with over 1000 plus CCTV installed and more
                            than 8000 alarms configured.</p>
                        <a class="main-slider__btn btn btn-success" href="#">LEARN MORE</a>
                        <div class="main-slider__control">
                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid block-content">
        <div class="row our-services">
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInLeft" data-wow-delay="0.3s">
                <a href="{{ url('/') }}">
                    <span><i class="glyph-icon flaticon-security47"></i></span>
                    <h4>Private security</h4>
                    We offer personalised security to clients and renowned and revered in the entire E.Africa
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInUp" data-wow-delay="0.3s">
                <a href="{{ url('/') }}">
                    <span><i class="glyph-icon flaticon-lock81"></i></span>
                    <h4>Guard house</h4>
                    We have trained specialist guards who are able to secure residential and commercial facilities.
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="{{ url('/') }}">
                    <span><i class="glyph-icon flaticon-video52"></i></span>
                    <h4>CCTV Installation</h4>
                    We install CCTV cameras to ensure that you have real time security in your premises.
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInLeft" data-wow-delay="0.3s">
                <a href="{{ url('/') }}">
                    <span><i class="glyph-icon flaticon-password10"></i></span>
                    <h4>Data protection</h4>
                    We have pioneered our systems to ensure we protect our clients data at all costs.
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInUp" data-wow-delay="0.3s">
                <a href="{{ url('/') }}">
                    <span><i class="glyph-icon flaticon-emergency1"></i></span>
                    <h4>Transport security</h4>
                    We provide escort and monitoring services for all cars and trucks across E. Africa.
                </a>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 wow zoomInRight" data-wow-delay="0.3s">
                <a href="{{ url('/') }}">
                    <span><i class="glyph-icon flaticon-boxing13"></i></span>
                    <h4>Defense training</h4>
                    Train to better defend yourself in our one of a kind institution with world acclaimed trainers.
                </a>
            </div>
        </div>
    </div>


    <div class="info-texts wow zoomInUp clearfix" data-wow-delay="0.3s">
        <div class="col-sm-4 col-md-4 col-lg-4">
            <span class="box-col1">Everybody. Everywhere. Everyday.</span>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
            <span class="box-col2">{{ config('bravo.bravo.slogan2') }}</span>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
            <span class="box-col3">{{ config('bravo.bravo.slogan1') }}</span>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row column-info block-content">
            <div class="col-sm-4 col-md-4 col-lg-4 wow fadeInLeft" data-wow-delay="0.3s">
                <div class="img-hover-effect">
                    <a href="{{ url('/') }}"><img src="{{ asset('frontend/media/3-column-info/1.jpg') }}" alt="slider"></a>
                </div>
                <span></span>
                <h3>24/7 CONSTANT SUPPORT</h3>
                <p>We provide all round support to all our clients and the community in Kenya. In case of emergecies,
                    contact us for assistance</p>
                <a class="btn btn-default btn-sm" href="{{ url('/') }}">READ MORE</a>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                <div class="img-hover-effect">
                    <a href="{{ url('/') }}"><img src="{{ asset('frontend/media/3-column-info/2.jpg') }}" alt="slider"></a>
                </div>
                <span></span>
                <h3>ONLY PROFESSIONALS</h3>
                <p>We hire trained proffesionals, ex-millitary and ex-police are part of our staff. This we do to ensure
                    maximum security to our clients and the community at large.</p>
                <a class="btn btn-default btn-sm" href="{{ url('/') }}">READ MORE</a>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 wow fadeInRight" data-wow-delay="0.3s">
                <div class="img-hover-effect">
                    <a href="{{ url('/') }}"><img src="{{ asset('frontend/media/3-column-info/3.jpg') }}" alt="slider"></a>
                </div>
                <span></span>
                <h3>ONLY THE LATEST EQUIPMENT</h3>
                <p>We have the latest technologies and equipment at our disposal and also invest heavily in research and
                    development to ensure we keep at par with technology.</p>
                <a class="btn btn-default btn-sm" href="{{ url('/') }}">READ MORE</a>
            </div>
        </div>
    </div>


    <div class="fleet-gallery block-content bg-image inner-offset text-white">
        <div class="container-fluid inner-offset">
            <div class="text-center hgroup wow zoomInUp" data-wow-delay="0.3s">
                <h1>Our GALLERY</h1>
                <h2>Here is our gallery. Do browse it.</h2>
            </div>
            <div id="fleet-gallery" class="owl-carousel enable-owl-carousel" data-pagination="false"
                 data-navigation="true" data-min450="2" data-min600="2" data-min768="4">
                <div class="wow rotateIn img-hover-effect" data-wow-delay="0.3s"><img
                            src="{{ asset('frontend/media/fleet-gallery/1.jpg') }}"
                            alt="Img"></div>
                <div class="wow rotateIn img-hover-effect" data-wow-delay="0.3s"><img
                            src="{{ asset('frontend/media/fleet-gallery/2.jpg') }}"
                            alt="Img"></div>
                <div class="wow rotateIn img-hover-effect" data-wow-delay="0.3s"><img
                            src="{{ asset('frontend/media/fleet-gallery/3.jpg') }}"
                            alt="Img"></div>
                <div class="wow rotateIn img-hover-effect" data-wow-delay="0.3s"><img
                            src="{{ asset('frontend/media/fleet-gallery/4.jpg') }}"
                            alt="Img"></div>
                <div class="wow rotateIn img-hover-effect" data-wow-delay="0.3s"><img
                            src="{{ asset('frontend/media/fleet-gallery/5.jpg') }}"
                            alt="Img"></div>
                <div class="wow rotateIn img-hover-effect" data-wow-delay="0.3s"><img
                            src="{{ asset('frontend/media/fleet-gallery/2.jpg') }}"
                            alt="Img"></div>
            </div>
        </div>
    </div>


    <div class="container-fluid block-content">
        <div class="row">
            <div class="col-md-6 col-lg-6 wow fadeInLeft" data-wow-delay="0.3s">
                <div class="hgroup">
                    <h1>TRUSTED CLIENTS</h1>
                    <h2>Here is what clients had to say about us.</h2>
                </div>
                <div id="testimonials" class="owl-carousel enable-owl-carousel" data-single-item="true"
                     data-pagination="false" data-navigation="true" data-auto-play="true">
                    <div>
                        <div class="testimonial-content">
                            <span><i class="fa fa-quote-left"></i></span>
                            <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac
                                mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam
                                rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal
                                pellentesque bibendum.</p>
                        </div>
                        <div class="text-right testimonial-author">
                            <h4>JOHN DEO</h4>
                            <small>Managing Director</small>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial-content">
                            <span><i class="fa fa-quote-left"></i></span>
                            <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac
                                mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam
                                rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal
                                pellentesque bibendum.</p>
                        </div>
                        <div class="text-right testimonial-author">
                            <h4>JOHN DEO</h4>
                            <small>Managing Director</small>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial-content">
                            <span><i class="fa fa-quote-left"></i></span>
                            <p>Integer congue elit non semper laoreet sed lectus orci posuer nisl tempor se felis ac
                                mauris. Pelentesque inyd urna. Integer vitae felis vel magna posu du vestibulum. Nam
                                rutrum congue diam. Aliquam malesuada maurs etug met Curabitur laoreet convallis nisal
                                pellentesque bibendum.</p>
                        </div>
                        <div class="text-right testimonial-author">
                            <h4>JOHN DEO</h4>
                            <small>Managing Director</small>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-lg-6 wow fadeInRight" data-wow-delay="0.3s">
                <div class="hgroup">
                    <h1>WHY CHOOSE US</h1>
                    <h2>We give you reasons why we are the perfect guard company.</h2>
                </div>

                <div id="accordion-1" class="panel-group accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-1"
                                                      href="#collapse-1" class="btn-collapse"><i class="icon"></i></a>
                            <h3 class="panel-title">Privately Owned Security Firm.</h3>
                        </div>
                        <div id="collapse-1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>While this may seem like a small detail at first, think about the priorities of some
                                    of the national, publicly traded security companies. They have to please a board of
                                    directors, investors and stock analysts. Because Bravo Two Zero Security services is
                                    privately owned, we’re able to make satisfying our clients our top priority. We
                                    answer only to you, and we’ll never sacrifice quality to increase stock market
                                    dividends</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-1"
                                                      href="#collapse-2" class="btn-collapse collapsed"><i
                                        class="icon"></i></a>
                            <h3 class="panel-title">We listen to your needs.</h3>
                        </div>
                        <div id="collapse-2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Another advantage of being privately owned, locally based and consistently staffed is
                                    the ability to offer one-on-one attention. We’ll sit down with you to understand
                                    your needs, assess your vulnerabilities, and recommend the best response with a
                                    program that meets your security, public relations and budget objectives. We can
                                    also respond immediately to any problems or changes in your needs.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-1"
                                                      href="#collapse-3" class="btn-collapse collapsed"><i
                                        class="icon"></i></a>
                            <h3 class="panel-title">We Hire Only the Best Security Officers</h3>
                        </div>
                        <div id="collapse-3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Our pre-employment screening includes a thorough interview process, a complete
                                    background check, an honesty and drug profile, and a criminal history fingerprint
                                    check through the Department of Public Safety and CID. We offer an unparalleled
                                    classroom and field training program for our security officers which teaches proper
                                    procedures, state law, using good judgment, communication skills, code of conduct,
                                    courtesy and much more. We also periodically evaluate our employees’ needs and offer
                                    a continuing plan of education and training for additional skills.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-1"
                                                      href="#collapse-4" class="btn-collapse collapsed"><i
                                        class="icon"></i></a>
                            <h3 class="panel-title">You’ll Never Pay Too Much</h3>
                        </div>
                        <div id="collapse-4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Instead of using a rigid, standard rate sheet, at Sterling Protective Services we
                                    evaluate the individual needs of each potential client prior to preparing an actual
                                    security proposal. We consider all of the variables, including the complexity of
                                    your needs, geographic location, your budget objectives, duration and volume of
                                    service, and more. Once we understand your individual needs, we will prepare a
                                    comprehensive proposal along with a proposed hourly bill rate for our services. And
                                    this evaluation service is absolutely free.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="block-content bg-image blog-section inner-offset text-white">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="hgroup wow fadeInLeft" data-wow-delay="0.3s">
                        <h1>LATEST NEWS</h1>
                        <h2>READ our latest blog news</h2>
                    </div>
                    <a class="btn btn-danger pull-right read-all-news wow fadeInRight" data-wow-delay="0.3s"
                       href="{{ url('/') }}">READ ALL NEWS</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 one-news wow fadeInLeft" data-wow-delay="0.3s">
                    <div style="background-image:url(media/news-images/1.jpg);">
                        <div>
                            <a href="{{ url('/') }}"><h3>BODYGUARD LIVING OPEN DAYS BEHEMALE</h3></a>
                            <small class="news-author">BY ADMIN</small>
                            <small>{{ \Carbon\Carbon::today()->addDays(15)->toFormattedDateString() }}</small>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 two-news wow fadeInRight" data-wow-delay="0.3s">
                    <div class="news-item row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div style="background-image:url(media/news-images/2.jpg);"></div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div>
                                <a href="{{ url('/') }}"><h3>Green doesn’t seed fish female</h3></a>
                                <small class="news-author">BY ADMIN</small>
                                <small>JAN 13, 2016</small>
                            </div>
                        </div>
                    </div>
                    <div class="news-item row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div style="background-image:url(media/news-images/3.jpg);"></div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div>
                                <a href="{{ url('/') }}"><h3>Duis vel tellus vintent tincidun</h3></a>
                                <small class="news-author">BY ADMIN</small>
                                <small>JAN 13, 2016</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid partners block-content">
        <div class="hgroup title-space wow fadeInLeft" data-wow-delay="0.3s">
            <h1>TRUSTED partners</h1>
            <h2>I AM TEST TEXT BLOCK. CLICK EDIT BUTTON TO CHANGE THIS TEXT.</h2>
        </div>
        <div id="partners" class="owl-carousel enable-owl-carousel" data-pagination="false" data-navigation="true"
             data-min450="2" data-min600="2" data-min768="4">
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img
                            src="{{ asset('frontend/media/partners/1.png') }}" alt="Img"></a>
            </div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img
                            src="{{ asset('frontend/media/partners/2.png') }}" alt="Img"></a>
            </div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img
                            src="{{ asset('frontend/media/partners/3.png') }}" alt="Img"></a>
            </div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img
                            src="{{ asset('frontend/media/partners/4.png') }}" alt="Img"></a>
            </div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img
                            src="{{ asset('frontend/media/partners/1.png') }}" alt="Img"></a>
            </div>
            <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img
                            src="{{ asset('frontend/media/partners/2.png') }}" alt="Img"></a>
            </div>
        </div>
    </div>
@endsection