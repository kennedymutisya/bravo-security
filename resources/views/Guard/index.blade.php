@extends('layouts.bravo')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Guards</h3>
                <p class="text-muted m-b-30">All Active Guards</p>
                <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Employed On</th>
                            <th>Client</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var \App\Guards $guard */ ?>
                        @foreach ($guards as $guard)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('guard.show',$guard->id) }}">{{ $guard->name }}</a></td>
                                <td>{{ $guard->phonenumber }}</td>
                                <td>{{ $guard->email }}</td>
                                <td>{{ $guard->employedDate }}</td>
                                <td>{{ $guard->client->name }}</td>
                            </tr>
                        @endforeach
{{--                        {{ $guards->links }}--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
