@extends('layouts.bravo')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Guards</h3>
                <p class="text-muted m-b-30">All Unassigned Guards</p>
                <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Employed On</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var \App\Guards $guard */ ?>
                        @foreach ($guards as $guard)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('guard.show',$guard->id) }}">{{ $guard->name }}</a></td>
                                <td>{{ $guard->employedDate }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--<div class="pull-right">--}}
                        {{--{{ $guards->links() }}--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
