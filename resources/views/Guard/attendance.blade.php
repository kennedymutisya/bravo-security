@extends('layouts.bravo')
@section('title')
    Attendance
@stop
<?php /** @var \App\Guards $guard */ ?>
@section('content')
    <div class="row">
       <div class="col-md-12">

           <div class="card card-body">
               <h3 class="box-title m-b-0">Attendance</h3>
               <div class="row">
                   <div class="col-sm-12 col-xs-12">
                       <attendance :guards="{{ \App\Guards::whereHas('currentassignments')->with('client')->get() }}"></attendance>
                   </div>
               </div>
           </div>

       </div>
    </div>
@endsection