@extends('layouts.bravo')
@section('content')
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
            <h3 class="box-title">Payments</h3>
            <p class="text-muted m-b-30">Current Payments
                <span class="pull-right">
                <i class="fa fa-file-pdf-o"></i>
                <i class="fa fa-print" onclick="printPayments()"></i>
            </span>
            </p>

            <table class="table table-condensed table-hover">
                <thead>
                <tr>
                    <th>Client Name</th>
                    <th>Invoice No.</th>
                    <th>Current Month</th>
                    <th>Remaining Balance</th>
                    <th>Paid</th>
                </tr>
                </thead>
                <tbody>
                <?php /** @var \App\Payments $value */ ?>
                @foreach ($all as $value)
                    <tr>
                        <td>
                            <a href="{{ route('payments.show',$value->id) }}">{{ $value->clients->name }}</a>
                        </td>
                        <td>{{ $value->invoice }}</td>
                        <td>
                            <span class="label label-success">{{ $value->startofmonth->toFormattedDateString() }}</span>
                            &mdash;
                            <span class="label label-success">{{ $value->endofmonth->toFormattedDateString() }}</span>
                        </td>
                        <td>{{ number_format($value->amountpaid - $value->payment->sum('amount') ,2) }}</td>
                        <td>

                            {{ number_format($value->payment->sum('amount'),2) }}

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function printPayments() {
            window.location.assign("/admin/print/payments")
        }
    </script>
@endpush