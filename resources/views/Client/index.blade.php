@extends('layouts.bravo')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Clients</h3>
                <p class="text-muted m-b-30">All Active Clients</p>
                <clients :clients="{{ \App\Client::all() }}"></clients>
            </div>
        </div>
    </div>
@endsection
