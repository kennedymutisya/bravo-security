@extends('layouts.bravo')
@section('title')
    Add Client
@stop
@section('content')
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Add Client</h3>
            <p class="text-muted m-b-30 font-13"> Happy we are getting a new client. </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <form action="{{ route('client.store') }}" method="post">
                        {{ csrf_field() }}
                        {{-- Label:Name , Attributes:name  --}}
                        <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Name</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        {{-- Label:Email , Attributes:email  --}}
                        <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">Email</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-email"></i></div>
                                <input id="email" type="text" class="form-control" name="email"
                                       value="{{ old('email') }}">
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        {{-- Label:Phone Number , Attributes:phone_number  --}}
                        <div class="col-md-6 form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="control-label">Phone Number</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                <input id="phone_number" type="text" class="form-control" name="phone_number"
                                       value="{{ old('phone_number') }}">
                            </div>
                            @if ($errors->has('phone_number'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                            @endif
                        </div>
                        {{-- Label:Guards , Attributes:guards  --}}
                        <div class="col-md-6 form-group{{ $errors->has('guards') ? ' has-error' : '' }}">
                            <label for="guards" class="control-label">Guards</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-server"></i></div>
                                <input id="guards" type="text" class="form-control" name="guards"
                                       value="{{ old('guards') }}">
                            </div>
                            @if ($errors->has('guards'))
                                <span class="help-block">
                                <strong>{{ $errors->first('guards') }}</strong>
                            </span>
                            @endif
                        </div>
                        {{-- Label:Start Date , Attributes:start_date  --}}
                        <div class="col-md-6 form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                            <label for="start_date" class="control-label">Start Date</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-alarm-clock"></i></div>
                                <input id="start_date" type="text" class="form-control" name="start_date"
                                       value="{{ old('start_date') }}">
                            </div>
                            @if ($errors->has('start_date'))
                                <span class="help-block">
                                <strong>{{ $errors->first('start_date') }}</strong>
                            </span>
                            @endif
                        </div>

                        {{-- Label:End Date , Attributes:end_date  --}}
                        <div class="col-md-6 form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                            <label for="end_date" class="control-label">End Date</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-alarm-clock"></i></div>
                                <input id="end_date" type="text" class="form-control" name="end_date"
                                       value="{{ old('end_date') }}">
                            </div>
                            @if ($errors->has('end_date'))
                                <span class="help-block">
                                <strong>{{ $errors->first('end_date') }}</strong>
                            </span>
                            @endif
                        </div>
                        {{-- Label:Rate , Attributes:rate  --}}
                        <div class="col-md-6 form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
                            <label for="rate" class="control-label">Rate</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="ti-alarm-clock"></i></div>
                                <input id="rate" type="text" class="form-control" name="rate"
                                       value="{{ old('rate',334) }}">
                            </div>
                            @if ($errors->has('rate'))
                                <span class="help-block">
                                <strong>{{ $errors->first('rate') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Add Client
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection