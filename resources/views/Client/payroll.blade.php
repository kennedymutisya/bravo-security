<html>
<head>
    <title>Bravo Security</title>
    <script language="JavaScript" type="text/javascript">
        /*setTimeout("window.print();", 10000);*/
    </script>
    <style>
        body {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
        }

        table.data {
            font-family: Verdana;
            font-size: 9px;
            empty-cells: show;
            border: 1px solid #000;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        table.data td {
            border: 1px solid black;
        }

        table.data td.header {
            background-color: #EDECEB;
            font-size: 13px !important;
            font-weight: bold;
        }

        table.data td.abottom {
            vertical-align: bottom;
            /*font-size: 10px;*/
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px;
            height: 50px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }


    </style>

</head>
<body>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td colspan="9">

            <table width="100%" border=0 cellspacing="0" cellpadding="1" class="data">

                <tr>
                    <td rowspan="6" align="center">
                        <img src="{{ asset('BRAVOLOGO.png') }}" height="100px">
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2"><span class="title">Bravo Two Zero Security Service</span></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Address : P.O BOX 132 &mdash; 90138, Makindu</b></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Tel : 0722 393 529 | 0713 642 175</b></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Your security is our commitment! </b></td>
                </tr>

                <tr>
                    <td colspan="4"><b>https://bravosecurity.co.ke</b></td>
                </tr>

                <tr>
                    <td><b>PAYROLL:</b></td>
                    <td colspan="4"><b> AUGUST 2018</b></td>
                </tr>

            </table>

        </td>
    </tr>

</table>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr nobr="true">
        <td nowrap class="header"></td>
        <td nowrap class="header">CLIENT</td>
        <td nowrap class="header">DAYS WORKED</td>
        <td nowrap class="header">AMOUNT</td>
        <td nowrap class="header">DEDUCTIONS</td>
        <td nowrap class="header">SIGNATURE</td>
    </tr>
    <tbody>

    @foreach($client as $clients)
        <tr>
            <td colspan="4">
                <strong>{{ strtoupper($clients->name) }}</strong>
                {{--<br>--}}
                <span style="float: right !important">
                    <strong>
                        {{ strtoupper($clients->guards) }}
                        Assigned {{ \Illuminate\Support\Str::plural('Guard',$clients->guards) }}</strong>
                </span>
               {{--<span style="float: right !important; ">--}}
                    {{--<br>--}}
                {{--<strong> KES {{ number_format($clients->guards * $clients->dailyrate) }}</strong>--}}
               {{--</span>--}}
            </td>
        </tr>
        @foreach($clients->assignment as $cl)
            <tr>
                <td align="right" valign="top">{{ $loop->iteration }}</td>
                <td valign="top">
                    {{ $cl->mkono->name }}
                </td>
                <td valign="top">{{ $cl->startDate->diffInDays($cl->endDate) + 1 }}</td>
                <td valign="top"><strong>KES </strong>{{number_format(($cl->days_worked) * 193.54,2) }}</td>
                <td valign="top">
                    KES 500 - UNIFORM
                    <br>
                    &nbsp;
                </td>
                <td></td>
            </tr>
        @endforeach
@endforeach
    <tr>
        <td colspan="3">
            <img src="https://koparahisi.loan/images/signatureKEN.png" alt="Ken Signature" width="46px" height="33px">
            <br>
            <span style="color: lightgray;">Printed On: {{ \Carbon\Carbon::now()->toDateTimeString() }}</span>
        </td>
        <td></td>
    </tr>
    </tbody>
</table>
</body>
</html>

