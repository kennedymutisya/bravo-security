@extends('layouts.bravo')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>

                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Client</th>
                                    <td>{{ $payment->clients->name }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Amount</th>
                                    <td>{{ $payment->amountpaid }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Month</th>
                                    <td>{{ $payment->startofmonth }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Invoice Number</th>
                                    <td>{{ $payment->invoice }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <update-payments :paymentid="{{ $payment->id }}"></update-payments>
                            </div>
                            <div class="col-md-6">
                                <update-invoice :invoiceid="{{ $payment->id }}"></update-invoice>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payment->payment as $item)

                                    <tr>
                                        <td scope="row">{{ $item->date }}</td>
                                        <td>{{ number_format($item->amount,2) }}</td>
                                    </tr>@endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection