<!DOCTYPE html>
<HTML lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bravo Two Zero Security Services</title>

    <link href="{{ asset('frontend/css/master.css') }}" rel="stylesheet">

    <!-- SWITCHER -->
    <link rel="stylesheet" id="switcher-css" type="text/css"
          href="{{ asset('frontend/assets/switcher/css/switcher.css') }}" media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('frontend/assets/switcher/css/color1.css') }}"
          title="color1" media="all"
          data-default-color="true"/>
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('frontend/assets/switcher/css/color2.css') }}"
          title="color2" media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('frontend/assets/switcher/css/color3.css') }}"
          title="color3" media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('frontend/assets/switcher/css/color4.css') }}"
          title="color4" media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('frontend/assets/switcher/css/color5.css') }}"
          title="color5" media="all"/>
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('frontend/assets/switcher/css/color6.css') }}"
          title="color6" media="all"/>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body data-scrolling-animations="true">
<div class="sp-body">
    <!-- Loader Landing Page -->
    <div id="ip-container" class="ip-container">
        <div class="ip-header">
            <div class="ip-loader">
                <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                    <path class="ip-loader-circlebg"
                          d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,39.3,10z"/>
                    <path id="ip-loader-circle" class="ip-loader-circle"
                          d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
    </div>
    <!-- Loader end -->
    <header id="this-is-top">
        <div class="container-fluid">
            <div class="topmenu row">
                <nav class="col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-sm-6 col-md-6 col-lg-6">
                    <a href="{{ url('about-us') }}">ABOUT US</a>
                    <a href="{{ url('our-service') }}">OUR SERVICE</a>
                    <a href="{{ url('contact-us') }}">CONTACT US</a>
                </nav>
                <nav class="text-right col-sm-3 col-md-3 col-lg-3">
                    <ul class="social-links">
                        <li><a href="http://www.bravosecurity.co.ke" target="_blank"><i
                                        class="social_icons fa fa-facebook-square"></i></a></li>
                        <li><a href="http://www.bravosecurity.co.ke" target="_blank"><i
                                        class="social_icons fa fa-youtube-square"></i></a></li>
                        <li><a href="http://www.bravosecurity.co.ke" target="_blank"><i
                                        class="social_icons fa fa-vimeo-square"></i></a></li>
                        <li><a href="http://www.bravosecurity.co.ke" target="_blank"><i
                                        class="social_icons fa fa-twitter-square"></i></a></li>
                        <li><a href="http://www.bravosecurity.co.ke" target="_blank"><i
                                        class="social_icons fa fa-google-plus-square"></i></a></li>
                        <li><a href="http://www.bravosecurity.co.ke" target="_blank"><i
                                        class="social_icons fa fa-tumblr-square"></i></a></li>
                    </ul>
                </nav>
            </div>
            <div class="header row">
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="{{ url('/') }}" class="logo" id="logo"><img src="{{ asset('frontend/img/logosite.png') }}" height="80px"
                                                                         alt="logo"></a>
                </div>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="text-right header-padding">
                        <div class="h-block"><span>EMAIL SUPPORT</span><a
                                    href="mailto:info@domain.com">info@bravosecurity.co.ke</a></div>
                        <div class="h-block"><span>CALL SUPPORT</span>0713642175</div>
                        <div class="h-block"><span>WORKING HOURS</span>Mon - Sat 0900 - 1900</div>
                        <a class="btn btn-success" href="{{ url('/') }}">Request Quote</a>
                    </div>
                </div>
            </div>
            <div id="main-menu-bg"></div>
            <a id="menu-open" href="#"><i class="fa fa-bars"></i></a>

        </div>
    </header>


    <nav class="main-menu navbar-main-slide">
        <div class="container-fluid">
            <ul class="nav navbar-nav navbar-main">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a href="{{ url('/about-us') }}">ABOUT US</a>
                </li>
                <li>
                    <a href="{{ url('/our-services') }}">Our Services</a>
                </li>
                <li>
                    <a href="{{ url('/our-services') }}">Contact Us</a>
                </li>
            </ul>

            <div class="search pull-right">
                <div class="header-search ">
                    <a href="#" class="btn_header_search"><i class="fa fa-search"></i></a>
                    <div class="search-form-modal transition">
                        <form class="search-form navbar-form header_search_form"
                              action="http://pix-theme.com/wordpress/safeguard" method="get">
                            <i class="fa fa-times search-form_close"></i>
                            <div class="form-group">
                                <input type="search" name="s" id="search" placeholder="Search" class="form-control"
                                       value="">
                            </div>
                            <button class="btn btn_search customBgColor" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <a id="menu-close" href="#"><i class="fa fa-times"></i></a>


    @yield('content')

    <footer>
        <div class="color-part2"></div>
        <div class="color-part"></div>
        <div class="block-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 wow zoomIn" data-wow-delay="0.3s">
                        <a href="#" class="logo-footer"></a>
                        <p>Bravo Two Zero is a leading security company based in Kenya with over 10 years in the
                            security sector.We have scooped various awards and are proud to be serving our clients and
                            meeting their needs and as we say: Ordinary days require exta-ordinary security.</p>
                        <div class="footer-icons">
                            <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                            <a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
                            <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                            <a href="#"><i class="fa fa-pinterest-square fa-2x"></i></a>
                            <a href="#"><i class="fa fa-vimeo-square fa-2x"></i></a>
                        </div>
                        <a href="#" class="footer-btn btn btn-lg btn-danger">GET A FREE QUOTE</a>
                    </div>
                    <div class="col-sm-2 wow zoomIn" data-wow-delay="0.3s">
                        <h4>WE OFFER</h4>
                        <ul class="footer-list list-unstyled">
                            <li><a href="#"> About us</a></li>
                            <li><a href="#"> Delivery Information</a></li>
                            <li><a href="#"> Terms &amp; Conditions</a></li>
                            <li><a href="#"> Privacy Policy</a></li>
                            <li><a href="#"> Contact us</a></li>
                            <li><a href="#"> Return Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 wow zoomIn" data-wow-delay="0.3s">
                        <h4>MAIN LINKS</h4>
                        <ul class="footer-list list-unstyled">
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="#">Terms &amp; Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Return Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 wow zoomIn" data-wow-delay="0.3s">
                        <h4>CONTACT INFO</h4>
                        Everyday is a new day for us and we work really hard to satisfy our customers everywhere.
                        <div class="contact-info">
                            <span><i class="fa fa-map-marker"></i><strong>Bravo Two Zero Security Systems.</strong><br>132 &mdash; 90138, Makindu, Kenya</span>
                            <span><i class="fa fa-phone"></i>+254 713 642 175</span>
                            <span><i class="fa fa-envelope"></i>info@bravosecurity.co.ke</span>
                            <span><i class="fa fa-clock-o"></i>Mon – Sat  8:00 – 19:00</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy text-center"> Copyrights © {{ date('Y') }} Bravo Two Zero Security | All rights reserved.</div>
    </footer>
</div>
<!--Main-->


<script src="{{ asset('frontend/js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/js/modernizr.custom.js') }}"></script>

<script src="{{ asset('frontend/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('frontend/js/waypoints.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.easypiechart.min.js') }}"></script>
<!-- Loader -->
<script src="{{ asset('frontend/assets/loader/js/classie.js') }}"></script>
<script src="{{ asset('frontend/assets/loader/js/pathLoader.js') }}"></script>
<script src="{{ asset('frontend/assets/loader/js/main.js') }}"></script>
<script src="{{ asset('frontend/js/classie.js') }}"></script>
<!--Switcher-->
<script src="{{ asset('frontend/assets/switcher/js/switcher.js') }}"></script>
<!--Owl Carousel-->
<script src="{{ asset('frontend/assets/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- SCRIPTS -->
<script type="text/javascript" src="{{ asset('frontend/assets/isotope/jquery.isotope.min.js') }}"></script>
<!--Theme-->
<script src="{{ asset('frontend/js/wow.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.placeholder.min.js') }}"></script>
<script src="{{ asset('frontend/js/theme.js') }}"></script>
</body>

</html>
