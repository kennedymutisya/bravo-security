<html>
<head>
    <title>Bravo Security</title>
    <script language="JavaScript" type="text/javascript">
        /*setTimeout("window.print();", 10000);*/
    </script>
    <style>
        body {
            background: #f0f0f0;
            width: 100vw;
            height: 100vh;
            display: flex;
            justify-content: center;
            padding: 20px;
            height: 100%;
            flex-wrap: wrap;

        }

        @import url('https://fonts.googleapis.com/css?family=Roboto:200,300,400,600,700');

        * {
            font-family: 'Roboto', sans-serif;
            font-size: 12px;
            color: #444;
        }

        #payslip {
            width: calc(8.5in - 80px);
            height: calc(11in - 60px);
            background: #fff;
            padding: 30px 40px;
        }

        #title {
            margin-bottom: 20px;
            font-size: 38px;
            font-weight: 600;
        }

        #scope {
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 7px 0 4px 0;
            display: flex;
            justify-content: space-around;
        }

        #scope > .scope-entry {
            text-align: center;
        }

        #scope > .scope-entry > .value {
            font-size: 14px;
            font-weight: 700;
        }

        .content {
            display: flex;
            border-bottom: 1px solid #ccc;
            height: 880px;
        }

        .content .left-panel {
            border-right: 1px solid #ccc;
            min-width: 200px;
            padding: 20px 16px 0 0;
        }

        .content .right-panel {
            width: 100%;
            padding: 10px 0 0 16px;
        }

        #employee {
            text-align: center;
            margin-bottom: 20px;
        }

        #employee #name {
            font-size: 15px;
            font-weight: 700;
        }

        #employee #email {
            font-size: 11px;
            font-weight: 300;
        }

        .details, .contributions, .ytd, .gross {
            margin-bottom: 20px;
        }

        .details .entry, .contributions .entry, .ytd .entry {
            display: flex;
            justify-content: space-between;
            margin-bottom: 6px;
        }

        .details .entry .value, .contributions .entry .value, .ytd .entry .value {
            font-weight: 700;
            max-width: 130px;
            text-align: right;
        }

        .gross .entry .value {
            font-weight: 700;
            text-align: right;
            font-size: 16px;
        }

        .contributions .title, .ytd .title, .gross .title {
            font-size: 15px;
            font-weight: 700;
            border-bottom: 1px solid #ccc;
            padding-bottom: 4px;
            margin-bottom: 6px;
        }

        .content .right-panel .details {
            width: 100%;
        }

        .content .right-panel .details .entry {
            display: flex;
            padding: 0 10px;
            margin: 6px 0;
        }

        .content .right-panel .details .label {
            font-weight: 700;
            width: 120px;
        }

        .content .right-panel .details .detail {
            font-weight: 600;
            width: 130px;
        }

        .content .right-panel .details .rate {
            font-weight: 400;
            width: 80px;
            font-style: italic;
            letter-spacing: 1px;
        }

        .content .right-panel .details .amount {
            text-align: right;
            font-weight: 700;
            width: 90px;
        }

        .content .right-panel .details .net_pay div, .content .right-panel .details .nti div {
            font-weight: 600;
            font-size: 12px;
        }

        .content .right-panel .details .net_pay, .content .right-panel .details .nti {
            padding: 3px 0 2px 0;
            margin-bottom: 10px;
            background: rgba(0, 0, 0, 0.04);
        }
        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }
    </style>
</head>
<body>
<div class="page-break"></div>

@foreach(\App\Assignment::with('mkono')->take(5)->get() as $client)
    <div>
        <div id="payslip">
            <div id="title">Payslip</div>
            <div id="scope">
                <div class="scope-entry">
                    <div class="title">PAY RUN</div>
                    <div class="value">Sep 5, 2018</div>
                </div>
                <div class="scope-entry">
                    <div class="title">PAY PERIOD</div>
                    <div class="value">Aug 1 - Aug 31, 2018</div>
                </div>
            </div>
            <div class="content">
                <div class="left-panel">
                    <div id="employee">
                        <div id="name">
                            {{ $client->mkono->name }}
                        </div>
                        <div id="email">
                            {{ $client->mkono->email }}
                        </div>
                    </div>
                    <div class="details">
                        <div class="entry">
                            <div class="label">Employee ID</div>
                            <div class="value">{{ $client->mkono->employmentID }}</div>
                        </div>
                        <div class="entry">
                            <div class="label">Monthly Rate</div>
                            <div class="value">
                                6,000
                            </div>
                        </div>
                        <div class="entry">
                            <div class="label">Company Name</div>
                            <div class="value">Bravo Two Zero Security System</div>
                        </div>
                        <div class="entry">
                            <div class="label">Date Hired</div>
                            <div class="value">{{$client->mkono->employedDate}}</div>
                        </div>
                        <div class="entry">
                            <div class="label">Position</div>
                            <div class="value">Point Guard</div>
                        </div>
                        <div class="entry">
                            <div class="label">Department</div>
                            <div class="value">1st String</div>
                        </div>
                        <div class="entry">
                            <div class="label">Rank</div>
                            <div class="value">MVP</div>
                        </div>
                        <div class="entry">
                            <div class="label">Payroll Cycle</div>
                            <div class="value">Monthly</div>
                        </div>

                        <div class="entry">
                            <div class="label">Prepared by</div>
                            <div class="value">Kennedy Mutisya</div>
                        </div>
                    </div>
                    <div class="gross">
                        <div class="title">Gross Income</div>
                        <div class="entry">
                            <div class="label"></div>
                            <div class="value">

                                {{ number_format(($client->endDate->diffInDays($client->startDate)+ 1)  * 193.54,2) }}
                            </div>
                        </div>
                    </div>
                    <div class="contributions">
                        <div class="title">Employer Contribution</div>
                        <div class="entry">
                            <div class="label">NSSF</div>
                            <div class="value">200</div>
                        </div>
                        <div class="entry">
                            <div class="label">NHIF</div>
                            <div class="value">300</div>
                        </div>

                    </div>
                    <div class="ytd">
                        <div class="title">Year To Date Figures</div>
                        <div class="entry">
                            <div class="label">Gross Income</div>
                            <div class="value">N/A</div>
                        </div>
                        <div class="entry">
                            <div class="label">Taxable Income</div>
                            <div class="value">N/A</div>
                        </div>
                        <div class="entry">
                            <div class="label">Withholding Tax</div>
                            <div class="value">N/A</div>
                        </div>
                        <div class="entry">
                            <div class="label">Net Pay</div>
                            <div class="value">N/A</div>
                        </div>
                        <div class="entry">
                            <div class="label">Allowance</div>
                            <div class="value">N/A</div>
                        </div>
                        <div class="entry">
                            <div class="label">Bonus</div>
                            <div class="value">N/A</div>
                        </div>
                        <div class="entry">
                            <div class="label">Commission</div>
                            <div class="value">N/A</div>
                        </div>
                        <div class="entry">
                            <div class="label">Deduction</div>
                            <div class="value">0.00</div>
                        </div>
                    </div>
                </div>
                <div class="right-panel">
                    <div class="details">
                        <div class="basic-pay">
                            <div class="entry">
                                <div class="label">Basic Pay</div>
                                <div class="detail"></div>
                                <div class="rate">5,999.74/Month</div>
                                <div class="amount">5,999.74</div>
                            </div>
                        </div>
                        <div class="salary">
                            <div class="entry">
                                <div class="label">Salary</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount"></div>
                            </div>
                            <div class="entry">
                                <div class="label"></div>
                                <div class="detail">Night</div>
                                <div class="rate">{{$client->endDate->diffInDays($client->startDate) + 1}} Days</div>
                                <div class="amount">{{ number_format(($client->endDate->diffInDays($client->startDate)+ 1)  * 193.54,2) }}</div>
                            </div>

                        </div>
                        <div class="leaves">
                            <div class="entry">
                                <div class="label">Leaves</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount"></div>
                            </div>
                            <div class="entry paid">
                                <div class="label"></div>
                                <div class="detail">Paid Leave</div>
                                <div class="rate">2 Days</div>
                                <div class="amount">(Incl.)</div>
                            </div>
                            <div class="entry unpaid">
                                <div class="label"></div>
                                <div class="detail">1 Day</div>
                                <div class="rate">--</div>
                                <div class="amount">(Incl.)</div>
                            </div>
                        </div>
                        <div class="taxable_allowance">
                            <div class="entry">
                                <div class="label">Taxable Allowance</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount"></div>
                            </div>
                            <div class="entry">
                                <div class="label"></div>
                                <div class="detail">Allowance Name</div>
                                <div class="rate"></div>
                                <div class="amount">0.00</div>
                            </div>
                        </div>
                        <div class="taxable_commission"></div>
                        <div class="contributions">
                            <div class="entry">
                                <div class="label">Contributions</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount"></div>
                            </div>
                            <div class="entry">
                                <div class="label"></div>
                                <div class="detail">BR20 Sacco</div>
                                <div class="rate"></div>
                                <div class="amount">(0.00)</div>
                            </div>
                            <div class="entry">
                                <div class="label"></div>
                                <div class="detail">Loan Fund</div>
                                <div class="rate"></div>
                                <div class="amount">(0.00)</div>
                            </div>
                            <div class="entry">
                                <div class="label"></div>
                                <div class="detail">H-Support</div>
                                <div class="rate"></div>
                                <div class="amount">(0.00)</div>
                            </div>
                        </div>
                        <div class="nti">
                            <div class="entry">
                                <div class="label">TAXABLE INCOME</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount">{{ number_format(($client->endDate->diffInDays($client->startDate)+ 1)  * 193.54,2) }}</div>
                            </div>
                        </div>
                        <div class="withholding_tax">
                            <div class="entry">
                                <div class="label">Withholding Tax</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount">(0.00)</div>
                            </div>
                        </div>
                        <div class="deductions">
                            <div class="entry">
                                <div class="label">Deductions</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount"></div>
                            </div>
                            <div class="entry">
                                <div class="label"></div>
                                <div class="detail">NSSF</div>
                                <div class="rate"></div>
                                <div class="amount">(0.00)</div>
                            </div>
                            <div class="entry">
                                <div class="label"></div>
                                <div class="detail">NHIF</div>
                                <div class="rate"></div>
                                <div class="amount">(0.00)</div>
                            </div>
                        </div>
                        <div class="net_pay">
                            <div class="entry">
                                <div class="label">NET PAY</div>
                                <div class="detail"></div>
                                <div class="rate"></div>
                                <div class="amount">{{ number_format(($client->endDate->diffInDays($client->startDate)+ 1)  * 193.54,2) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-break"></div>
@endforeach

</body>
</html>

