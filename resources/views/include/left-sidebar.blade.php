<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                            </span></div>
                <!-- /input-group -->
            </li>
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="{{ asset('plugins/images/users/1.jpg') }}" alt="user-img"
                                                      class="img-circle"> <span class="hide-menu">Kennedy Mutisya
                <span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
            <li class="nav-small-cap m-t-10">--- Main Menu</li>
            <li>
                <a href="{{ url('admin') }}" class="{{ Request::segment(1) == '' ? 'waves-effect active' : ''}}"><i
                            data-icon="&#xe008;"
                            class="linea-icon linea-basic fa-fw"></i> <span
                            class="hide-menu">Dashboard</span></a>
            </li>
            <li class="">
                <a href="{{ url('/') }}" class="{{ Request::segment(1) == 'client' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-user-md fa-fw"></i>
                    <span class="hide-menu">Client<span class="fa arrow"></span></span></a>

                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(2) == 'active' ? 'waves-effect active' : ''}}"><a
                                href="{{ route('activeclient') }}">Active Clients</a></li>
                    <li class="{{ Request::segment(2) == '' ? 'waves-effect active' : ''}}"><a
                                href="{{ route('client.index') }}">All Clients</a></li>
                    <li class="{{ Request::segment(2) == 'payments' ? 'waves-effect active' : ''}}"><a
                                href="{{ route('payments') }}">Payments</a></li>
                    <li class="{{ Request::segment(2) == 'create' ? 'waves-effect active' : ''}}"><a
                                href="{{ route('client.create') }}">Add Client</a></li>
                    <li class="{{ Request::segment(2) == 'inactive' ? 'waves-effect active' : ''}}"><a
                                href="{{ route('inactiveclient') }}">Lost Clients</a></li>
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/') }}" class="{{ Request::segment(1) == 'guard' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-user-secret fa-fw"></i>
                    <span class="hide-menu">Guard<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == '' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('guard.index') }}">Active Guards</a></li>
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(3) == 'unassigned' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('unassignedGuard') }}">Unassigned Guards</a>
                    </li>
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == 'create' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('guard.create') }}">Add</a></li>
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == 'transfer' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('transfer.index') }}">Transfers</a></li>
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == 'attendance' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('attendance.index') }}">Attendance</a></li>
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/') }}" class="{{ Request::segment(1) == 'report' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-file-pdf-o fa-fw"></i>
                    <span class="hide-menu">Reports<span class="fa arrow"></span>
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == '' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('reports.index') }}">MD Report</a>
                    </li>
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == 'transfer' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('transfer.index') }}">Payroll</a>
                    </li>
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == 'attendance' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('attendance.index') }}">Client Remittance</a>
                    </li>
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == 'attendance' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('attendance.index') }}">Attendance</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/') }}" class="{{ Request::segment(1) == 'report' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-life-bouy fa-fw"></i>
                    <span class="hide-menu">NHIF<span class="fa arrow"></span>
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == '' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('reports.index') }}">NHIF</a>
                    </li>

                </ul>
            </li>
            <li class="">
                <a href="{{ url('/') }}" class="{{ Request::segment(1) == 'report' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-legal fa-fw"></i>
                    <span class="hide-menu">NSSF<span class="fa arrow"></span>
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(1) == 'guard' && Request::segment(2) == '' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('reports.index') }}">NSSF</a>
                    </li>

                </ul>
            </li>
        </ul>
    </div>
</div>