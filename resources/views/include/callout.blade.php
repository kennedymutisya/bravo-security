<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">{{ $slot }}</h3>
        </div>
    </div>
</div>