@extends('layouts.bravo')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Transfer Guards</h3>
                <p class="text-muted m-b-30">Transfer guards to new stations</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <transfer :client="{{ \App\Client::all() }}" :guard="{{ \App\Guards::all() }}"></transfer>
            </div>
        </div>
    </div>
@endsection
