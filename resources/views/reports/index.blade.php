@extends('layouts.bravo')
@section('title')
    Reports
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Reports</h3>
                    </div>
                    <div class="panel-body">
                        <!-- TAB NAVIGATION -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#tab1" role="tab" data-toggle="tab">MD Report</a></li>
                            <li><a href="#tab2" role="tab" data-toggle="tab">Payroll</a></li>
                            <li><a href="#tab3" role="tab" data-toggle="tab">Attendance</a></li>
                        </ul>
                        <!-- TAB CONTENT -->
                        <div class="tab-content">
                            <div class="active tab-pane fade in" id="tab1">
                                <form action="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="month" class="col-sm-2 control-label">Month</label>

                                            <select class="form-control" id="month">
                                                <option value="">Aug, 2018</option>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-file-pdf-o"></i>
                                                PDF
                                            </button>
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-print"></i>
                                                Print
                                            </button>
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-eye"></i>
                                                View
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="tab2">
                                <form action="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="month" class="col-sm-2 control-label">Month</label>

                                            <select class="form-control" id="month">
                                                <option value="">Aug, 2018</option>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-file-pdf-o"></i>
                                                PDF
                                            </button>
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-print"></i>
                                                Print
                                            </button>
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-eye"></i>
                                                View
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="tab-pane fade" id="tab3">
                                <form action="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="month" class="col-sm-2 control-label">Month</label>

                                            <select class="form-control" id="month">
                                                <option value="">Aug, 2018</option>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-file-pdf-o"></i>
                                                PDF
                                            </button>
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-print"></i>
                                                Print
                                            </button>
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-eye"></i>
                                                View
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection